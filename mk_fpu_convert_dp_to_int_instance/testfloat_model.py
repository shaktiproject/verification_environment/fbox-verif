import subprocess
import shlex
import os
from enum import Enum
from datetime import datetime
from random import randint

def sys_command(command):
    x = subprocess.Popen(shlex.split(command),
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         )
    try:
        out, err = x.communicate(timeout=5)
    except TimeoutExpired:
        x.kill()
        out, err = x.communicate()

    out = out.rstrip()
    err = err.rstrip()
    return out.decode("ascii")

def run_testfloat(function, round_mode, seed, timeout_value, test_file=''):

    #global out,err
    pk = sys_command('which pk')
    testfloat_gen = sys_command('which testfloat_gen')
    if (seed == 0):
        seed = randint(1, 650000)
    #command = 'spike {0} {1} -r{2} -seed {3} -level 2 {4}'.format(pk, testfloat_gen, 
    #                                                 round_mode, seed, function)
    if test_file:
        fp = open(test_file, 'r')
        gen = fp.read()
        fp.close()
    else:
        #command = 'qemu-riscv64 {0} {1}'.format( testfloat_gen, function)
        command = 'qemu-riscv64 {0} -r{1} -seed {2}  -level 2 {3} -exact'.format( testfloat_gen, 
                                                         round_mode, seed, function)
        print(command)
        proc = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        gen = ''
        try:
            out,err = proc.communicate(timeout=timeout_value)
        except subprocess.TimeoutExpired:
            proc.kill()
        
        gen = out.decode('ascii')
        stampnow = datetime.now()
        filename = 'tf_{0}_{1}_{2}_{3}.test'.format(function, seed,round_mode, stampnow.strftime("%Y%m%d%H%M%S%f"))
        fp = open(filename, 'w')
        fp.write(gen)
        fp.close()
    
    tests = gen.splitlines()
    #print(tests)

    input_list = []
    expected_list = []
    for test in tests:
        inp = ''
        if test != 'bbl loader':
            tlist = test.split(' ')
            #print(tlist)
            #if (function=='f62_to_ui32'):
                
            inp = bin(int((tlist[0]), 16))[2:]   #input from testfloat
            flag = bin(int((tlist[2]), 16))[2:]  #flag bits from testfloat 8 bit
            flag=flag.zfill(8)
            flag=flag[3:]     #truncate flag 5bit
            inp=inp.zfill(64) #64bit flooating point inp
            inp_msb=inp[:1] #sign 63rd bit
            inp_sfd_bits=inp[12:]   #sfd 0-51bit
            inp_sfd_msb=inp_sfd_bits[:1]  #52nd bit
            inp_exp_1=inp[:12]   
            inp_exp_2=inp_exp_1[1:] #exponent bit 11bit[52-62th]
            if(inp_exp_2=='11111111111') and (inp_sfd_msb=='0') and (inp_sfd_bits!='0000000000000000000000000000000000000000000000000000'):
                 isSNaN=1
                 #print("isSNaN")
     
            else:
                 isSNaN=0

            if(inp_exp_2=='11111111111') and (inp_sfd_msb=='1'):
                 isQNaN=1
                 #print("isQNaN")
            else:
                 isQNaN=0
            if(inp_exp_2=='11111111111') and (inp_sfd_bits=='0000000000000000000000000000000000000000000000000000'):
                 A1=1
            else:
                 A1=0
            if(inp=='0000000000000000000000000000000000000000000000000000000000000000') or (inp=='1000000000000000000000000000000000000000000000000000000000000000'):
                 A3=1
            else:
                 A3=0
            if(inp_exp_2=='00000000000') and (inp_sfd_bits!='0000000000000000000000000000000000000000000000000000'):
                 A4=1
            else:
                 A4=0
            A0=isSNaN | isQNaN
            A2=0

            exception=[A4,A3,A2,A1,A0]
            e = ''.join(map(str, exception))
            #print("\n\ninp sfd         ",inp_sfd_bits)
            #print("inp sfd msb     ",inp_sfd_msb)
            #print("inp exponent bit",inp_exp_2)
            #print("exception bit",e)
            #print("inp             ",inp)
            #print("inp msb             ",inp_msb)
            #print("flag             ",flag)
            if(round_mode=='near_even') :
                inp=int(inp+'000'+e,2)

            if(round_mode=='near_maxMag') :
                inp=int(inp+'100'+e,2)

            if(round_mode=='minMag') :
                inp=int(inp+'001'+e,2)

            if(round_mode=='min') :
                inp=int(inp+'010'+e,2)

            if(round_mode=='max') :
                inp=int(inp+'011'+e,2)

            if (function=='f64_to_i32'):
                
                exp = bin(int((tlist[1]), 16))[2:]
                value=exp.zfill(32) 
                exp=exp.zfill(64) 
                exp_msb=value[:1] 
                if exp_msb=='1':
                    exp=int(exp,2) | 0xFFFFFFFF00000000
                    exp=bin(exp)[2:]
                exp=int('1'+exp+flag,2)
                input_list.append(inp)
                expected_list.append(exp)



            if (function=='f64_to_ui32'):
                
                exp = bin(int((tlist[1]), 16))[2:]
                value=exp.zfill(32) 
                exp=exp.zfill(64) 
                exp_msb=value[:1] 
                if exp_msb=='1':
                    exp=int(exp,2) | 0xFFFFFFFF00000000
                    exp=bin(exp)[2:]
                exp=int('1'+exp+flag,2)
                input_list.append(inp)
                expected_list.append(exp)


            if (function=='f64_to_ui64'):
                
                exp = bin(int((tlist[1]), 16))[2:]
                value=exp.zfill(64) 
                exp=exp.zfill(64) 
                exp_msb=value[:1] 
                exp=int('1'+exp+flag,2)
                input_list.append(inp)
                expected_list.append(exp)

            if (function=='f64_to_i64'):
                
                exp = bin(int((tlist[1]), 16))[2:]
                value=exp.zfill(64) 
                exp=exp.zfill(64) 
                exp_msb=value[:1] 
                exp=int('1'+exp+flag,2)
                input_list.append(inp)
                expected_list.append(exp)


    
    return input_list, expected_list

if __name__ == "__main__":
    run_testfloat('f64_to_ui64', 'min', 10000,5)
