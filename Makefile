.PHONY: all
all: test

.PHONY: clean
clean:
	-@find . -name "obj" | xargs rm -rf
	-@find . -name "*.pyc" | xargs rm -rf
	-@find . -name "*results.xml" | xargs rm -rf
	$(MAKE) -C mk_fpu_add_sub_dp_instance clean
	$(MAKE) -C mk_fpu_add_sub_sp_instance clean
	$(MAKE) -C mk_fpu_compare_dp_instance clean
	$(MAKE) -C mk_fpu_compare_sp_instance clean
	$(MAKE) -C mk_fpu_convert_dp_to_int_instance clean
	$(MAKE) -C mk_fpu_convert_dp_to_sp_instance clean
	$(MAKE) -C mk_fpu_convert_int_to_dp_instance clean
	$(MAKE) -C mk_fpu_convert_int_to_sp_instance clean
	$(MAKE) -C mk_fpu_convert_sp_to_dp_instance clean
	$(MAKE) -C mk_fpu_convert_sp_to_int_instance clean
	$(MAKE) -C mk_fpu_max_dp_instance clean
	$(MAKE) -C mk_fpu_max_sp_instance clean
	$(MAKE) -C mk_fpu_min_dp_instance clean
	$(MAKE) -C mk_fpu_min_sp_instance clean
	$(MAKE) -C mk_fpu_multiplier_dp_instance clean
	$(MAKE) -C mk_fpu_multiplier_sp_instance clean

.PHONY: do_tests
do_tests::
	$(MAKE) -k -C mk_fpu_add_sub_dp_instance
do_tests::
	$(MAKE) -k -C mk_fpu_add_sub_sp_instance
do_tests::
	$(MAKE) -k -C mk_fpu_compare_dp_instance
do_tests::
	$(MAKE) -k -C mk_fpu_compare_sp_instance
do_tests::
	$(MAKE) -k -C mk_fpu_convert_dp_to_int_instance
do_tests::
	$(MAKE) -k -C mk_fpu_convert_dp_to_sp_instance
do_tests::
	$(MAKE) -k -C mk_fpu_convert_int_to_dp_instance
do_tests::
	$(MAKE) -k -C mk_fpu_convert_int_to_sp_instance
do_tests::
	$(MAKE) -k -C mk_fpu_convert_sp_to_dp_instance
do_tests::
	$(MAKE) -k -C mk_fpu_convert_sp_to_int_instance
do_tests::
	$(MAKE) -k -C mk_fpu_max_dp_instance
do_tests::
	$(MAKE) -k -C mk_fpu_max_sp_instance
do_tests::
	$(MAKE) -k -C mk_fpu_min_dp_instance
do_tests::
	$(MAKE) -k -C mk_fpu_min_sp_instance
do_tests::
	$(MAKE) -k -C mk_fpu_multiplier_dp_instance
do_tests::
	$(MAKE) -k -C mk_fpu_multiplier_sp_instance 

# For Jenkins we use the exit code to detect compile errors or catastrophic
# failures and the XML to track test results
.PHONY: jenkins
jenkins: do_tests
	./bin/combine_results.py --suppress_rc --testsuites_name=cocotb_regression

# By default want the exit code to indicate the test results
.PHONY: test
test:
	$(MAKE) do_tests; ret=$$?; ./bin/combine_results.py && exit $$ret

COCOTB_MAKEFILES_DIR = $(realpath $(shell cocotb-config --makefiles))
AVAILABLE_SIMULATORS = $(patsubst .%,%,$(suffix $(wildcard $(COCOTB_MAKEFILES_DIR)/simulators/Makefile.*)))

.PHONY: help
help:
	@echo ""
	@echo "This cocotb makefile has the following targets"
	@echo ""
	@echo "all, test - run regression producing combined_results.xml"
	@echo "            (return error code produced by sub-makes)"
	@echo "jenkins   - run regression producing combined_results.xml"
	@echo "            (return error code 1 if any failure was found)"
	@echo "clean     - remove build directory and all simulation artefacts"
	@echo ""
	@echo "The default simulator is Icarus Verilog."
	@echo "To use another, set the environment variable SIM as below."
	@echo "Available simulators:"
	@for X in $(sort $(AVAILABLE_SIMULATORS)); do \
		echo export SIM=$$X; \
	done
	@echo ""
